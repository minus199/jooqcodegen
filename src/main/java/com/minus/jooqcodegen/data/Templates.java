package com.minus.jooqcodegen.data;

/**
 *
 * @author MiNuS420 <minus199@gmail.com>
 */
public enum Templates {

    BASE_MODEL("BaseModel.ftl"),
    TABLES("Tables.ftl");
    private final String fileName;

    Templates(String fileName) {
        this.fileName = fileName;
    }

    public String id() {
        return fileName;
    }

}
