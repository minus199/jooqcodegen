package com.minus.jooqcodegen.data;

import java.io.File;
import org.jooq.util.h2.H2Database;
import org.jooq.util.mysql.MySQLDatabase;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
public class Properties {

    public enum PATHS {

        OUTPUT_DIR("");

        private final String namespace;

        private PATHS(String namespace) {
            this.namespace = namespace;
        }

        final public String path() {
            return namespace;
        }

    }

    public enum PKG {

//        BASE_PKG("com.minus.soundfog"),
//        DB(BASE_PKG.ns() + ".modules.db"),
//        OUTPUT_PKG(DB.ns() + ".mapping");
        
        BASE_PKG("com.onehourtranslation.db"),
        DB(BASE_PKG.ns()),
        OUTPUT_PKG(DB.ns() + ".mapping");

        private final String namespace;

        private PKG(String namespace) {
            this.namespace = namespace;
        }

        final public String ns() {
            return namespace;
        }
    }

    public enum DB {

        //TYPE(H2Database.class.getName()),
        //DRIVER("org.h2.Driver"),
        //URL("jdbc:h2:/workspace/MiNuS/SoundFog/data/Sounds"),
        //USERNAME("app"),
        //PASSWORD("app");
        
        TYPE(MySQLDatabase.class.getName()),
        DRIVER("com.mysql.jdbc.Driver"),
        URL("jdbc:mysql://127.0.0.1/trans"),
        USERNAME("oht_app"),
        PASSWORD("aaa");

        private final String value;

        private DB(String value) {
            this.value = value;
        }

        public String value() {
            return value;
        }
    }
}
