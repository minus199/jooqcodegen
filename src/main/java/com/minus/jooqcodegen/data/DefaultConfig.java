package com.minus.jooqcodegen.data;

import com.minus.jooqcodegen.CustomStrategy;
import com.minus.jooqcodegen.Generator;
import org.jooq.util.jaxb.*;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
public final class DefaultConfig extends Configuration {
    public DefaultConfig() {
        withJdbc();
        withGenerator();

    }

    private static final class GeneratorConfig extends org.jooq.util.jaxb.Generator {

        public GeneratorConfig() {

            withTarget();
            withGenerate();
            withDatabase();
            withStrategy();
            withName();
        }

        public org.jooq.util.jaxb.Generator withTarget() {
            return super.withTarget(new Target()
                    .withPackageName(Properties.PKG.OUTPUT_PKG.ns())
                    .withDirectory(Properties.PATHS.OUTPUT_DIR.path())
            );
        }

        public org.jooq.util.jaxb.Generator withGenerate() {
            return super.withGenerate(new Generate()
                    .withDaos(Boolean.TRUE)
                    .withFluentSetters(Boolean.TRUE)
                    .withInstanceFields(Boolean.TRUE)
                    .withJpaAnnotations(Boolean.TRUE)
                    .withPojos(Boolean.TRUE)
                    .withRelations(Boolean.TRUE)
                    .withValidationAnnotations(Boolean.TRUE)
                    .withGeneratedAnnotation(Boolean.FALSE)
                    .withInterfaces(Boolean.TRUE)); //To change body of generated methods, choose Tools | Templates.
        }

        public org.jooq.util.jaxb.Generator withDatabase() {
            return super.withDatabase(new Database()
                    .withSchemata(new Schema().withInputSchema("trans").withOutputSchema("trans").withOutputSchemaToDefault(false))
                    .withName(Properties.DB.TYPE.value())
            );
        }

        public org.jooq.util.jaxb.Generator withStrategy() {
            return super.withStrategy(new Strategy().withName(CustomStrategy.class.getName())); //To change body of generated methods, choose Tools | Templates.
        }

        public org.jooq.util.jaxb.Generator withName() {
            return super.withName(Generator.class.getName()); //To change body of generated methods, choose Tools | Templates.
        }

    }

    public Configuration withGenerator() {
        return super.withGenerator(new GeneratorConfig());
    }

    public Configuration withJdbc() {
        return super.withJdbc(new Jdbc()
                .withDriver(Properties.DB.DRIVER.value())
                .withUrl(Properties.DB.URL.value())
                .withUser(Properties.DB.USERNAME.value())
                .withPassword(Properties.DB.PASSWORD.value())
        );
    }

}
