package com.minus.jooqcodegen;

import com.minus.jooqcodegen.subgenerators.TablesCodeGenerator;
import com.minus.jooqcodegen.data.DefaultConfig;
import com.minus.jooqcodegen.data.Properties;
import com.minus.jooqcodegen.subgenerators.BaseModelGenerator;
import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jooq.util.GenerationTool;
import org.jooq.util.JavaGenerator;
import org.jooq.util.JavaWriter;
import org.jooq.util.SchemaDefinition;

/**
 * @author minus
 */
public class Generator extends JavaGenerator {

    public static void main(String[] args) {
        try {
            Class.forName(Properties.DB.DRIVER.value());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Generator.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            GenerationTool.generate(new DefaultConfig());
        } catch (Exception ex) {
            Logger.getLogger(Generator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void empty(File file, String suffix, Set<File> keep) {
        if ("BaseModel.java".equals(file.getName()) && !keep.contains(file)) {
            keep.add(file);
        }

        super.empty(file, suffix, keep); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void generatePojos(SchemaDefinition schema) {
        super.generatePojos(schema);
        new BaseModelGenerator(getStrategy(), schema).generate();
    }

    @Override
    protected void generateTableReferences(SchemaDefinition schema) {
        CustomWriter out = new CustomWriter(new File(getStrategy().getFile(schema).getParentFile(), "Tables.java"));

        try {
            TablesCodeGenerator tablesCodeGenerator = new TablesCodeGenerator(schema, strategy);
            tablesCodeGenerator.generate();
            out.println(tablesCodeGenerator.getGenerated());
        } catch (ClassNotFoundException | SQLException | FileNotFoundException ex) {
            Logger.getLogger(Generator.class
                    .getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        } catch (Exception ex) {
            Logger.getLogger(Generator.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeJavaWriter(out);
        }
    }

    private class CustomWriter extends JavaWriter {

        public CustomWriter(File file) {
            super(file, null);
        }

        @Override
        public List<String> ref(List<String> clazz, int keepSegments) {
            return super.ref(clazz, keepSegments); //To change body of generated methods, choose Tools | Templates.
        }

        public final String _ref(String clazz) {
            return super.ref(clazz);
        }

    }
}
