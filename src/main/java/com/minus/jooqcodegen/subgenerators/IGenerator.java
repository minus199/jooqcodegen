package com.minus.jooqcodegen.subgenerators;

/**
 *
 * @author asafb
 */
public interface IGenerator {
    public void generate() throws Exception;
    public String getGenerated();
}
