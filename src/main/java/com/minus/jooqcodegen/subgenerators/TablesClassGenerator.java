package com.minus.jooqcodegen.subgenerators;

import com.github.javaparser.ASTHelper;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.ModifierSet;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.body.VariableDeclaratorId;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import java.util.Arrays;
import org.jooq.util.GeneratorStrategy;
import org.jooq.util.SchemaDefinition;

/**
 * @author MiNuS420 <minus199@gmail.com>
 * @deprecated 
 */
class TablesClassGenerator {

    CompilationUnit generate(SchemaDefinition schema, GeneratorStrategy strategy) {

        CompilationUnit tablesCU = new CompilationUnit();
        String javaPackageName = strategy.getJavaPackageName(schema.getSchema(), GeneratorStrategy.Mode.DEFAULT);
        tablesCU.setPackage(new PackageDeclaration(ASTHelper.createNameExpr(javaPackageName)));

        /* Imports */
        /*        String[] importsString = new String[]{"java.lang.reflect.Field", "java.util.logging.Level", "java.util.logging.Logger",
         "com.minus.soundfog.modules.db.mappings.TableAliases", "java.util.stream.Collectors", "org.jooq.Table"};
         List<ImportDeclaration> imports = Stream.of(importsString).map(p -> new ImportDeclaration(ASTHelper.createNameExpr(p), false, false)).collect(Collectors.toList());
         imports.add(new ImportDeclaration(ASTHelper.createNameExpr("java.util"), false, true));
         tablesCU.setImports(imports);*/
        /* Class Decleration */
        
        final ClassOrInterfaceDeclaration type = new ClassOrInterfaceDeclaration(ModifierSet.PUBLIC, false, "Tables");
        ASTHelper.addTypeDeclaration(tablesCU, type);
        //type.setExtends(Arrays.asList(new ClassOrInterfaceType(TablesAbstract.class.getCanonicalName())));

        schema.getDatabase().getTables(schema).stream().forEach(t -> {
            FieldDeclaration fieldDeclaration = new FieldDeclaration(
                    ModifierSet.addModifier(ModifierSet.FINAL, ModifierSet.addModifier(ModifierSet.PUBLIC, ModifierSet.STATIC)),
                    new ClassOrInterfaceType(strategy.getFullJavaClassName(t)),
                    new VariableDeclarator(new VariableDeclaratorId(strategy.getJavaIdentifier(t)), new NameExpr(strategy.getFullJavaIdentifier(t))));

            ASTHelper.addMember(type, fieldDeclaration);
        });

        return tablesCU;
    }
}
