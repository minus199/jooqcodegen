package com.minus.jooqcodegen.subgenerators;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
import com.minus.jooqcodegen.TemplateEngine;
import com.minus.jooqcodegen.data.Properties;
import com.minus.jooqcodegen.data.Templates;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import org.jooq.util.GeneratorStrategy;
import org.jooq.util.SchemaDefinition;

public class TablesCodeGenerator implements IGenerator {

    private final SchemaDefinition schema;
    private final GeneratorStrategy strategy;
    private String template;

    /**
     *
     * @param schema
     * @param strategy
     */
    public TablesCodeGenerator(SchemaDefinition schema, GeneratorStrategy strategy) {
        this.schema = schema;
        this.strategy = strategy;
    }

    @Override
    public void generate() throws Exception {
        Class.forName(Properties.DB.DRIVER.value());

        Map<String, HashMap<String, String>> tablesData = schema.getDatabase().getTables(schema).stream().collect(
                Collectors.toMap(t -> strategy.getJavaIdentifier(t), t -> {
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("type", strategy.getFullJavaClassName(t));
                    hashMap.put("modifiers", String.join(" ", "public", "static", "final"));
                    hashMap.put("tableName", strategy.getJavaIdentifier(t));
                    hashMap.put("fullTable", strategy.getFullJavaIdentifier(t));

                    return hashMap;
                }));

        Map<String, Object> templateMap = new HashMap();
        templateMap.put("pkg", strategy.getJavaPackageName(schema.getSchema(), GeneratorStrategy.Mode.DEFAULT));
        templateMap.put("data", tablesData);

        this.template = TemplateEngine.getInstance().getTemplate(Templates.TABLES, templateMap);
    }

    @Override
    public String getGenerated() {
        return this.template;
    }

    

}
