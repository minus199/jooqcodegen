package com.minus.jooqcodegen.subgenerators;

import com.minus.jooqcodegen.Generator;
import com.minus.jooqcodegen.TemplateEngine;
import com.minus.jooqcodegen.data.Templates;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jooq.util.GeneratorStrategy;
import org.jooq.util.SchemaDefinition;

/**
 *
 * @author asafb
 */
public class BaseModelGenerator implements IGenerator {

    private final GeneratorStrategy strategy;
    private final SchemaDefinition schema;
    private String template;

    public BaseModelGenerator(GeneratorStrategy strategy, SchemaDefinition schema) {
        this.strategy = strategy;
        this.schema = schema;
    }

    @Override
    public void generate() {
        File modelFile = null;
        try {
            modelFile = createFile();
        } catch (RuntimeException ignoreAndReturn) {
            return; //no creation needed if already exists
        }

        try (PrintWriter out = new PrintWriter(modelFile)) {
            HashMap<String, String> data = new HashMap();
            data.put("pkg", strategy.getJavaPackageName(schema, GeneratorStrategy.Mode.DEFAULT) + ".tables.pojos");

            template = TemplateEngine.getInstance().getTemplate(Templates.BASE_MODEL, data);
            out.println(template);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Generator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getGenerated() {
        return template;
    }
    
    private File createFile() throws RuntimeException {
        File temp = strategy.getFile(schema, GeneratorStrategy.Mode.POJO);
        List<String> pathParts = Arrays.asList(temp.getAbsolutePath().split(File.separator));
        String pojosDir = String.join(File.separator, pathParts.subList(0, pathParts.size() - 2)) + File.separator + "tables" + File.separator + "pojos";
        String modelPath = pojosDir + File.separator + "BaseModel.java";
        
        //listenOnPojosDir(Paths.get(pojosDir));

        File modelFile = new File(modelPath);
        if (modelFile.exists()){
            throw new RuntimeException(modelFile.getPath() + " already exists. Skipping.");
        }
        
        try {
            modelFile.createNewFile();
        } catch (IOException ex) {
            Logger.getLogger(Generator.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("unable to create base model file", ex);
        }
        
        return modelFile;
    }

    static <T> WatchEvent<T> cast(WatchEvent<?> event) {
        return (WatchEvent<T>) event;
    }

    private void listenOnPojosDir(Path dir) {
        new Thread(() -> {
            try {
                WatchService watcher = FileSystems.getDefault().newWatchService();

                WatchKey key = dir.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);

                for (;;) {
                    for (WatchEvent<?> event : key.pollEvents()) {
                        WatchEvent.Kind kind = event.kind();

                        if (kind == OVERFLOW) {
                            continue;
                        }

                        WatchEvent<Path> ev = cast(event);
                        Path name = ev.context();
                        Path child = dir.resolve(name);

                        System.out.format("%s: %s\n", event.kind().name(), child);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(Generator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }

    
}
