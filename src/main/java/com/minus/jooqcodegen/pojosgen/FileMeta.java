package com.minus.jooqcodegen.pojosgen;

import com.github.javaparser.ASTHelper;
import com.github.javaparser.ast.PackageDeclaration;
import com.minus.jooqcodegen.data.Properties;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FilenameUtils;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
public class FileMeta {

    private static final Logger logger = Logger.getLogger(FileMeta.class.getName());

    private final String modelFileSpecificSimpleName;
    private final String absoluteFileName;
    private final String fulllyQualifiedName;
    private final String pkg;
    private final File modelFile;
    private final Class pojoClass;//Class<? extends BaseModel> pojoClass;
    private final File pojoFile;

    public FileMeta(File pojoFile) {
        this(null, pojoFile);
    }

    public FileMeta(Class/*<? extends BaseModel>*/ pojoClass, File pojoFile) {
        if (pojoClass == null) {
            String fullyQualifiedName = FilenameUtils.removeExtension(pojoFile.getPath()).substring("src/main/java/".length()).replaceAll(File.separator, "\\.");
            try {
                //pojoClass = (Class<? extends BaseModel>) ClassLoader.getSystemClassLoader().loadClass(fullyQualifiedName);
                throw new ClassNotFoundException("not reallyh");
            } catch (ClassNotFoundException ex) {
                java.util.logging.Logger.getLogger(FileMeta.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (pojoClass == null) {
                    throw new RuntimeException("unable to find class for pojo");
                }
            }
        }

        this.pojoClass = pojoClass;

        modelFileSpecificSimpleName = "Model" + pojoClass.getSimpleName();
        absoluteFileName = Properties.PATHS.MODELS_DIR.path().concat(File.separator).concat(modelFileSpecificSimpleName);
        fulllyQualifiedName = absoluteFileName.replaceAll(File.separator, "\\.").replace("src.main.java.", "");
        pkg = fulllyQualifiedName.replace("." + modelFileSpecificSimpleName, "");
        modelFile = new File(absoluteFileName.concat(".java"));
        this.pojoFile = pojoFile;
    }

    final public File createIfNotExists() {
        if (getModelFile().exists()) {
            return getModelFile(); // If file exists, do not override
        }

        return create();
    }

    final public File create() {
        logger.info("processing " + pojoClass.getSimpleName() + " to " + getFulllyQualifiedName()); // Not found, creating new

        try {
            getModelFile().createNewFile();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

        return getModelFile();
    }

    public String getModelName() {
        return modelFileSpecificSimpleName;
    }

    public String getAbsoluteFileName() {
        return absoluteFileName;
    }

    public String getFulllyQualifiedName() {
        return fulllyQualifiedName;
    }

    public PackageDeclaration getPkg() {
        return new PackageDeclaration(ASTHelper.createNameExpr(pkg));
    }

    public String getPkgString() {
        return pkg;
    }

    public File getModelFile() {
        return modelFile;
    }

    public File getPojoFile() {
        return pojoFile;
    }

    public Class<? extends BaseModel> getPojoClass() {
        return pojoClass;
    }

    public Class getModelClass() {
        try {
            return Class.forName(fulllyQualifiedName);
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FileMeta.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
