package com.minus.jooqcodegen.pojosgen;

import com.github.javaparser.ASTHelper;
import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseException;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.ModifierSet;
import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.minus.soundfog.modules.db.mappings.models.BaseModel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.apache.commons.io.FilenameUtils;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
class SuperPojoGenerator {

    private static final Logger logger = Logger.getLogger(SuperPojoGenerator.class.getName());

    private final Boolean forceSubtypesCreation;
    private final String fullyQualifiedName;
    private final Class<? extends BaseModel> pojoClass;
    private final FileMeta fileMeta;
    private CompilationUnit pojoCU;

    public SuperPojoGenerator(File pojoFile) throws ClassNotFoundException {
        this(pojoFile, true);
    }

    public SuperPojoGenerator(File pojoFile, Boolean forceSubtypesCreation) throws ClassNotFoundException {
        /* All for pojo, not model */

        this.forceSubtypesCreation = forceSubtypesCreation;
        fullyQualifiedName = FilenameUtils.removeExtension(pojoFile.getPath()).substring("src/main/java/".length()).replaceAll(File.separator, "\\.");
        pojoClass = (Class<? extends BaseModel>) ClassLoader.getSystemClassLoader().loadClass(fullyQualifiedName);

        fileMeta = new FileMeta(pojoClass, pojoFile);

        try {
            FileInputStream in = new FileInputStream(fileMeta.getPojoFile());

            try {
                pojoCU = JavaParser.parse(in);
            } finally {
                in.close();
            }
        } catch (ParseException | IOException parseException) {
        }
    }

    final public SuperPojoGenerator generate() {
        if (pojoClass.getDeclaredAnnotation(Table.class) == null || pojoClass.getDeclaredAnnotation(Entity.class) == null) {
            return this;
        }

        File modelFile = forceSubtypesCreation ? fileMeta.create() : fileMeta.createIfNotExists();

        try (PrintWriter out = new PrintWriter(modelFile)) {
            out.println(createModelClass());
            logger.info("processing finished for " + pojoClass.getSimpleName());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SuperPojoGenerator.class.getName()).log(Level.SEVERE, null, ex);

        }

        return this;
    }

    private CompilationUnit createModelClass() {
        CompilationUnit modelClass = new CompilationUnit();
        modelClass.setPackage(fileMeta.getPkg());
        ClassOrInterfaceDeclaration type = new ClassOrInterfaceDeclaration(ModifierSet.PUBLIC, false, fileMeta.getModelName());
        type.setExtends(new ArrayList() {
            {
                add(new ClassOrInterfaceType(pojoClass.getCanonicalName()));
            }
        });

        modelClass.setImports(pojoCU.getImports());
        ASTHelper.addTypeDeclaration(modelClass, type);

        ConstructorDeclaration allArgsConstructor = new ConstructorDeclaration(ModifierSet.PUBLIC, fileMeta.getModelName());
        try {
            allArgsConstructor.setParameters(extractConstructorArgs());
            ASTHelper.addMember(type, allArgsConstructor);

            // each table must start with 
            /*List<Expression> paramExperssions =  = new ArrayList() {
             {
             add(null);
             addAll(allArgsConstructor.getParameters().stream().map(t -> ASTHelper.createNameExpr(t.getId().getName())).collect(Collectors.toList()));
             add(null);
             }
             };
             BlockStmt constructorBlock = new BlockStmt();
             ASTHelper.addStmt(constructorBlock, new MethodCallExpr(null, "super", paramExperssions));
             allArgsConstructor.setBlock(constructorBlock);*/
        } catch (Exception ex) {
            Logger.getLogger(SuperPojoGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }

        ConstructorDeclaration noArgsConstructor = new ConstructorDeclaration(ModifierSet.PUBLIC, fileMeta.getModelName());
        noArgsConstructor.setBlock(new BlockStmt());
        ASTHelper.addMember(type, noArgsConstructor);

        ConstructorDeclaration thatConstructor = new ConstructorDeclaration(ModifierSet.PUBLIC, fileMeta.getModelName());
        Parameter param_ = ASTHelper.createParameter(ASTHelper.createReferenceType(fileMeta.getFulllyQualifiedName(), 0), fileMeta.getModelName().toLowerCase());
        thatConstructor.setParameters(new ArrayList() {
            {
                add(param_);
            }
        });
        BlockStmt thatBlock = new BlockStmt();
        ASTHelper.addStmt(thatBlock, new MethodCallExpr(null, "super", Arrays.asList(ASTHelper.createNameExpr(fileMeta.getModelName().toLowerCase()))));
        thatConstructor.setBlock(thatBlock);
        ASTHelper.addMember(type, thatConstructor);

        return modelClass;
    }

    private List<Parameter> extractConstructorArgs() throws Exception {
        // getTypes return all the classes which are declared inside the given class
        // Since only pojos are expected, the list of types will be considered as a one item list.
        // Therefore returning only one list of args
        List<String> ignores = Arrays.asList("setid", "setuuid"); // since the tables generates the id and basemodel generates the uuid

        return pojoCU.getTypes().get(0).getMembers().stream()
                .filter(m -> m instanceof MethodDeclaration && ((MethodDeclaration) m).getName().startsWith("set") && !ignores.contains(((MethodDeclaration) m).getName().toLowerCase()))
                .map(m -> ((MethodDeclaration) m).getParameters().get(0)) // each setter accepts one argument
                .collect(Collectors.toList());
    }

}
