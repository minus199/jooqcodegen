package com.minus.jooqcodegen.pojosgen;

import com.minus.jooqcodegen.data.Properties;
import java.io.File;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
class SuperPojosGenerator {

    private static final Logger logger = Logger.getLogger(SuperPojosGenerator.class.getName());

    public static void generate(Boolean forceSubtypesCreation) {
        validateDirectories();

        File[] pojoFiles = new File(new File(Properties.PATHS.MODELS_DIR.path()).getParentFile().getPath() + "/public_/tables/pojos").listFiles();

        Arrays.stream(pojoFiles).forEach((File f) -> {
            try {
                new SuperPojoGenerator(f).generate();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(SuperPojosGenerator.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        logger.info("Build complete.");
    }

    private static void validateDirectories() {
        File dir = new File(Properties.PATHS.MODELS_DIR.path());
        if (!dir.exists()) {

            dir.mkdirs();
            logger.info("Created directories recursively to " + dir.getPath());
        }
    }

}
