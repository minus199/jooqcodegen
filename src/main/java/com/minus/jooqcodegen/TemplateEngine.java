package com.minus.jooqcodegen;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
import com.minus.jooqcodegen.data.Templates;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Mantha Pavan Kumar
 * 
*/
public class TemplateEngine {

    Configuration configuration;

    public TemplateEngine() {
        configuration = new Configuration();
    }

    public static TemplateEngine getInstance() {
        File loadingDirectory = new File("src/main/resources/templates");
        
        try {
            TemplateEngine engine = new TemplateEngine();
            engine.configuration.setDirectoryForTemplateLoading(loadingDirectory);
            return engine;
        } catch (IOException ex) {
            Logger.getLogger(TemplateEngine.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    public String getTemplate(Templates tempalte, Map params) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        OutputStreamWriter output = new OutputStreamWriter(bos);
        Template template = null;
        try {
            template = configuration.getTemplate(tempalte.id());
            template.process(params, output);
            return bos.toString();
        } catch (IOException | TemplateException ex) {
            Logger.getLogger(TemplateEngine.class.getName()).log(Level.SEVERE, null, ex);
        }

        throw new RuntimeException("Unable to process.");
    }

}
