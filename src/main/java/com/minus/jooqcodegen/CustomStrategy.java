package com.minus.jooqcodegen;

import com.minus.jooqcodegen.data.Properties;
import org.jooq.util.DefaultGeneratorStrategy;
import org.jooq.util.Definition;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
public class CustomStrategy extends DefaultGeneratorStrategy {

    @Override
    public String getJavaClassExtends(Definition definition, Mode mode) {
        String defaultExtends = super.getJavaClassExtends(definition, mode);

        if (mode == Mode.POJO) {
            return "BaseModel";
        }

        return defaultExtends;
    }
    
    

}
