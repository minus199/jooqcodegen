package ${pkg};

import java.lang.reflect.InvocationTargetException;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Table;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
public abstract class BaseModel {

    private String name;

    {
        setUuid();
    }

    public final BaseModel setUuid() {
        // done with reflection in order to no make uuid mandatory for every basemodel (ie every table)
        try {
            getClass().getMethod("setUuid", String.class).invoke(this, UUID.randomUUID().toString());
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(BaseModel.class.getName()).log(Level.SEVERE, null, ex);
        }

        return this;
    }

    public BaseModel as(String name) {
        this.name = name;
        return this;
    }

    final public String getTableName() {
        try {
            return getClass().getAnnotation(Table.class).name();
        } catch (Exception e) {
            throw new RuntimeException("Table not found.");
        }
    }
}
