package ${pkg};

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.apache.commons.lang.ArrayUtils;
import org.jooq.Table;

/**
 *
 * @author MiNuS420 <minus199@gmail.com>
 */
public class Tables{

    private static final List<Field> tables;
    private static final List<String> tableNames;
        
    static {
        tables = Arrays.asList((Field[]) ArrayUtils.addAll(Tables.class.getDeclaredFields(), TableAliases.class.getDeclaredFields()));
        tableNames = tables.stream().map(t -> t.getName()).distinct().collect(Collectors.toList());
    }
    
    <#list data?keys as tableName> 
    ${data[tableName].modifiers} ${data[tableName].type} ${tableName} = ${data[tableName].fullTable};
    ${"\n"}
    </#list>
 
    final public static Boolean isTableExists(String name) {
        return tableNames.contains(name);
    }

    public static final Table tableForName(String name) {
        if (isTableExists(name)) {
            try {
                return (Table) tables.get(tableNames.indexOf(name)).get(null);
            } catch (IllegalArgumentException | IllegalAccessException | SecurityException ex) {
                Logger.getLogger(Tables.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        throw new RuntimeException("table not found.");
    }
}