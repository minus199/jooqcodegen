package ${mapping_pkg};

import com.minus.soundfog.modules.db.mappings.TableAliases;
import com.minus.soundfog.modules.db.mappings.public_.Tables;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.apache.commons.lang.ArrayUtils;
import org.jooq.Table;


/**
 *
 * @author MiNuS420 <minus199@gmail.com>
 */
public class Tables implements ${ITable} {

    private static final List<Field> tables;
    private static final List<String> tableNames;
        
    static {
        tables = Arrays.asList((Field[]) ArrayUtils.addAll(Tables.class.getDeclaredFields(), TableAliases.class.getDeclaredFields()));
        tableNames = tables.stream().map(t -> t.getName()).distinct().collect(Collectors.toList());
    }
    
    public static final com.minus.soundfog.modules.db.mappings.tables.Artist ARTIST = com.minus.soundfog.modules.db.mappings.tables.Artist.ARTIST;

    public static final com.minus.soundfog.modules.db.mappings.tables.FetchedPages FETCHED_PAGES = com.minus.soundfog.modules.db.mappings.tables.FetchedPages.FETCHED_PAGES;

    public static final com.minus.soundfog.modules.db.mappings.tables.Image IMAGE = com.minus.soundfog.modules.db.mappings.tables.Image.IMAGE;
 
    public static final com.minus.soundfog.modules.db.mappings.tables.RuntimeMeta RUNTIME_META = com.minus.soundfog.modules.db.mappings.tables.RuntimeMeta.RUNTIME_META;

    public static final com.minus.soundfog.modules.db.mappings.tables.Sound SOUND = com.minus.soundfog.modules.db.mappings.tables.Sound.SOUND;

    public static final com.minus.soundfog.modules.db.mappings.tables.SoundArtwork SOUND_ARTWORK = com.minus.soundfog.modules.db.mappings.tables.SoundArtwork.SOUND_ARTWORK;
                                                                                                   com.minus.soundfog.modules.db.mappings.tables.SoundArtwork.SOUND_ARTWORK
    public static final com.minus.soundfog.modules.db.mappings.tables.SoundExtras SOUND_EXTRAS = com.minus.soundfog.modules.db.mappings.tables.SoundExtras.SOUND_EXTRAS;

    public static final com.minus.soundfog.modules.db.mappings.tables.SoundStatistics SOUND_STATISTICS = com.minus.soundfog.modules.db.mappings.tables.SoundStatistics.SOUND_STATISTICS;

    public static final com.minus.soundfog.modules.db.mappings.tables.SoundUrls SOUND_URLS = com.minus.soundfog.modules.db.mappings.tables.SoundUrls.SOUND_URLS;

    public static final com.minus.soundfog.modules.db.mappings.tables.SoundWaveform SOUND_WAVEFORM = com.minus.soundfog.modules.db.mappings.tables.SoundWaveform.SOUND_WAVEFORM;

    final public static Boolean isTableExists(String name) {
        return tableNames.contains(name);
    }

    public static final Table tableForName(String name) {
        if (isTableExists(name)) {
            try {
                return (Table) tables.get(tableNames.indexOf(name)).get(null);
            } catch (IllegalArgumentException | IllegalAccessException | SecurityException ex) {
                Logger.getLogger(Tables.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        throw new RuntimeException("table not found.");
    }
}